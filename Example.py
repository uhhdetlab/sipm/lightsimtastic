from LightSimtastic import SiPMSimulation
import matplotlib.pyplot as plt
import numpy as np

#################################
# THE SIMULATION TOOL TAKES A DICTIONARY AS AN INPUT, WITH EACH OF THE VARIABLES AS KEYS.
# FOR EACH ELEMENT YOU ADD TO THE LISTS, ANOTHER SIMULATION WILL BE PERFORMED. 
# THIS ALLOWS SCANS THROUGH PARAMETERS.
#################################

variables={
"Name_Sim":[0], #THIS CAN BE ANYTHING YOU LIKE
"mu":[7], # MEAN NUMBER OF GEIGER DISCHARGES
"ppXT":[0.0], # PROBABILITY OF PROMPT CROSS-TALK 
"pdXT":[0.2], #PROBABILITY OF DELAYED CROSS-TALK
"taudXT":[25], #TIME CONSTANT FOR DELAYED CROSS-TALK (NS)
"rdXT":[0.5], #FRACTION OF DELAYED CROSS-TALK FROM ADJACENT CELLS
"pAp":[0.15], #PROBABILITY OF AFTERPULSE
"tauAp":[7.5], #TIME CONSTANT OF AFTERPULSE
"taur":[20], # SIPM RECOVERY TIME CONSTANT
"Sig0":[0.075], #WIDTH OF PEDESTAL [NORMALISED ADC]
"Sig1":[0.02], # INCREASE IN PEAK WIDTH PER ADDITIONAL DISCHARGE [NORMALISED ADC]
"DCR":[0.0], # DARK COUNT RATE [GHZ]
"Sigt":[0.02], # ELECTRONICS NOISE FOR CURRENT TRANSIENT (FOR TRANSIENT)
"GSig":[0.75], # WIDTH OF ELECTRONICS NOISE TRANSFER FUNCTION (FOR TRANSIENT)
"tslow":[20], # TIME CONSTANT FOR SLOW PART OF PULSE
"tfast":[1.5], # TIME CONSTANT FOR FAST PART OF PULSE
"rfast":[0.2], # PROPORTION OF SLOW/FAST
"start_tgate":[-5], # GATE START TIME [NS]
"len_tgate":[100], # LENGTH OF GATE
"t0":[100,], # STARTING POINT OF GATE
"tl0":[0], #GAUSSIAN MEAN OF PRIMARY GEIGER DICHARGE TIME DISTRIBUTION
"tl1":[0.1], #GAUSSIAN WIDTH OF PRIMARY GEIGER DICHARGE TIME DISTRIBUTION
"tl2":[0], #FREE PARAMETER
"Gen_mu":["Poisson"], # NUMBER PRIMARY GEIGER DISCHARGE PDF
"Gen_tmu":["Gauss"], # TIME OF PRIMARY GEIGER DISCHARGE PDF
"Gen_gain":["Gauss"],  # GAIN PDF (FOR TRANSIENT)
"Gen_npXT":["Binomial"], #NUMBER PROMPT X-TALK DISCHARGE DISTRIBUTION
"Gen_ndXT":["Binomial"],  #NUMBER PROMPT X-TALK DISCHARGE DISTRIBUTION
"Gen_tdXT":["Exp"], #TIME DELAYED X-TALK DISTRIBUTION
"Gen_nAP":["Binom"],  #NUMBER AFTER DISTRIBUTION
"Gen_tAP":["Exp"],  #AFTERPULSE TIME DISTRIBUTION
"Gen_noise":["Gauss"] #ELECTRONIC NOISE DISTRIBUTION (FOR TRANSIENT)
}

#################################
# CREATE A SIPM SIMULATION CLASS OBJECT
#################################
        
s = SiPMSimulation()

#################################
# ADD VARIABLES
#################################

s.AddVariables(variables)

n_events = int(1e5)


#################################
# SIMULATE
#################################

s.Simulate(n_events, transients=False)

#################################
# EXTRACT SIMULATION DATAFRAME
#################################

df = s.pars

#################################
# EXTRACT CHARGE SPECTRUM
#################################

Qs = df.iloc[0]["ChargeSpectrum"]

#################################
# PLOT
#################################

plt.figure(figsize=(15.5,10.5))
H, edges = np.histogram(Qs, bins=1000)
edges = edges[:-1]+(edges[1]-edges[0])
plt.plot(edges, H)
plt.title("Simulated Charge Spectrum")
plt.yscale("log")
plt.xticks(fontsize=25)
plt.yticks(fontsize=25)
plt.xlabel("# GD", fontsize=25),
plt.savefig("./Example.png")
                
