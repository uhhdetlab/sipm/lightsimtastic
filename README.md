# LightSimtastic

A Silicon Photo-Multiplier simulation package written in Python. This program can be used to simulate primary Geiger discharges and generate correlated Geiger discharges due to prompt and delayed cross-talk and after-pulses. 

A number of different physics-based models and statistical treatments for the simulation of correlated Geiger discharges can be selected. Inputs to the program are the mean number and the time distribution of Geiger discharges from photons, as well as the dark-count rate. 

By simulating many events, charge spectra - as measured by a current-integrating charge-to-digital converter - can be produced. In addition, current transients convolved with electronics response functions, as recorded by a digital oscilloscope, and time-difference distributions can be simulated. 



To get started, have a look at ```Example.py``` as well as ```ExampleTransients.py```.


## References

E. Garutti, R. Klanner, J. Rolph, J. Schwandt: _Simulation of the response of SiPMs; Part I: Without saturation effects_.  [doi.org/10.1016/j.nima.2021.165853](https://doi.org/10.1016/j.nima.2021.165853)
